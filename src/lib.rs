extern crate serialize;
pub mod vec2d
{	
	use std::ops::Add;
	use std::ops::Mul;
	use std::ops::Sub;
	use std::ops::Div;
	use std::fmt;
	use std::num::Float;
	/**
		Simple 2d vec for stadium ball
	**/
	
	#[derive(Clone)]
	#[derive(Copy)]
	#[derive(Encodable)]
	#[derive(Decodable)]
	pub struct Particle
	{
        	pub m: f64,
        	pub r: V2d,
        	pub v: V2d,
	}
	
	#[derive(Decodable)]
	#[derive(Encodable)]
	#[derive(Copy)]
	#[derive(Clone)]
	pub struct V2d
	{
		pub x: f64,
		pub y: f64,
	}
	impl V2d
	{
		pub fn equals(&self, other :&V2d)->bool
		{
			self.x==other.x && self.y==other.y
		}
		
		pub fn new(x: f64,y: f64) -> V2d
		{
			V2d
			{	x:x, 
				y:y,
			}
				
		}

		pub fn dist(&self, passed: &V2d) -> f64
		{
			(self - passed).mag()	
		}
		pub fn distsq(&self, passed: &V2d) -> f64
		{
			(self - passed).magsq()	
		}
		pub fn mag(&self)-> f64
		{
			(self * self).sqrt()	
		}
		pub fn magsq(&self) -> f64
		{
			self * self
		}
		/**Projects self onto passed**/
		pub fn proj(&self, passed: &V2d) -> V2d
		{
			//v2d   (f64/f64)
			passed*( (self*passed) / (passed*passed))
		}
	}
	impl<'a,'b>  Add<&'b V2d> for &'a V2d
	{
		type Output = V2d;
		
		fn add(self, _rhs: &V2d) -> V2d
		{
			V2d::new(self.x+_rhs.x,self.y+_rhs.y)
		}
	}
	impl<'a,'b> Sub<&'b V2d> for&'a V2d
	{
		type Output = V2d;
		
		fn sub(self, _rhs: &V2d) -> V2d
		{
			V2d::new(self.x-_rhs.x,self.y-_rhs.y)
		}
	}
	/**Dot product**/
	impl<'a,'b> Mul<&'b V2d> for &'a V2d
	{
		type Output= f64;
		
		fn mul(self,_rhs: &V2d) -> f64
		{
			self.x*_rhs.x + self.y*_rhs.y
		}
	}
	/**Scaling**/
	impl<'a> Mul<f64> for &'a V2d
	{
		type Output = V2d;
		
		fn mul(self, _rhs:f64)->V2d
		{
			V2d::new(self.x*_rhs,self.y*_rhs)
		}
	}
	impl<'a> Div<f64> for &'a V2d
	{
		type Output = V2d;
		
		fn div(self, _rhs:f64)->V2d
		{
			V2d::new(self.x/_rhs,self.y/_rhs)
		}
	}

	impl fmt::Display for V2d
	{
		fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result
		{
			write!(f,"<{}, {}>",self.x,self.y)		
		}
	}
}
